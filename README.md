# ncmpvc (ncurses mpv client)

## About

This is a fork of [Jonas Frei's ncmpvc](https://gitlab.com/mpv-ipc/ncmpvc). It's a ncurses client for mpv which connects to existing mpv instances through sockets, written in Rust.

This tool is inspired by ncmpcpp, a curses based client for the Music Player Daemon. It makes use of mpv's JSON IPC protocol to control any mpv instance over a given socket.

![ncmpvc screenshot](https://i.imgur.com/uFr5MHo.png)

Make sure mpv is started with the following option:
`
$ mpv --input-ipc-server=/tmp/mpvsocket ...
`

## Dependencies

- `mpv`
- `cargo` (makedep)
- `ncurses`

## Install

- [Arch](https://aur.archlinux.org/packages/ncmpvc-git) - `yaourt ncmpvc-git`

If you have packaged mpvc for your distribution, let me know so I can add it here.

### Manual Install

Use "cargo build --release" to build the program.
The output binary will be found in 'target/release/'

## Usage

Make sure mpv is started with the following option:
`
$ mpv --input-ipc-server=/tmp/mpvsocket --idle
`

At the moment ncmpvc does not launch mpv instances, so the instances have to be launched beforehand. Also, the path to the socket is hardcoded to `/tmp/mpvsocket`. It will be possible in the future to read this from a configuration file.
I'm not sure yet where to go with this project so this might change in the future.
To control mpv without a user interface I suggest the use of [mpvc](https://gitlab.com/mpv-ipc/mpvc-rs).

### Key bindings
Key | Feature | Comment
--- | --- | ---
Play | `ENTER` |
Scrolling | `UP`, `DOWN`, `PGUP`, `PGDOWN` |
Jump to the currently playing entry | `o`
Jump to an entry by it's number | `g` |
Shuffle playlist | `z` | mpv >= v0.26.0
Remove from playlist | `d` |
Toggle playback | `p` |
Toggle mute | `m` |
Search mode | `/` |
Cancel search mode | `ESC` |
Play next/previous song | `,`, `.` |
Volume up/down 2% | `=`, `-` |
Speed up/down 5% | `]`, `[` |
Seek (+/- 5 seconds) | `LEFT`, `RIGHT` |
Force playlist update | `u` | should never be necessary
Quit ncmpvc | `q`, `F10` |
